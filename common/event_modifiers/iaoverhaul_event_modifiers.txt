#Paradox "helpfully" HIDES the modifiers in the HRE window whenever they happen to cancel each other out exactly.
#After all, if various modifiers add up to zero, nobody will ever want to know why and how that happened, right? /s
#To fix this BUG-in-design, we add a tiny amount to each modifier value, in such a way that
#no (non-mutually-exclusive!) combination of modifiers can result in a net value of zero (cf. binary flags).

iaoverhaul.territorialintegrity = {
	imperial_authority_value = 0.10000001
}
iaoverhaul.severeterritorialloss = {
	imperial_authority_value = -0.30000001
}
iaoverhaul.significantterritorialloss = {
	imperial_authority_value = -0.20000001
}
iaoverhaul.someterritorialloss = {
	imperial_authority_value = -0.10000001
}



iaoverhaul.wartoreclaimimperialterritory = {
	imperial_authority_value = 0.10000002
}



iaoverhaul.religiousuniformity = {
	imperial_authority_value = 0.05000004
}
iaoverhaul.manyhereticelectors = {
	imperial_authority_value = -0.20000004
}
iaoverhaul.ahereticelector = {
	imperial_authority_value = -0.10000004
}
iaoverhaul.severeheresy = {
	imperial_authority_value = -0.20000008
}
iaoverhaul.significantheresy = {
	imperial_authority_value = -0.10000008
}
iaoverhaul.someheresy = {
	imperial_authority_value = -0.05000008
}

iaoverhaul.waragainstreligiousenemy = {
	imperial_authority_value = 0.05000016
}
iaoverhaul.peaceofwestphalia = {
	imperial_authority_value = -0.05000016
}



iaoverhaul.lovedbyallelectors = {
	imperial_authority_value = 0.10000032
}
iaoverhaul.lovedbymostelectors = {
	imperial_authority_value = 0.05000032
}
iaoverhaul.notlikedbyallelectors = {
	imperial_authority_value = -0.05000032
}
iaoverhaul.hatedbyanelector = {
	imperial_authority_value = -0.10000032
}
iaoverhaul.hatedbyelectors = {
	imperial_authority_value = -0.20000032
}



iaoverhaul.legitimateemperor = {
	imperial_authority_value = 0.05000064
}
iaoverhaul.illegitimateemperor = {
	imperial_authority_value = -0.10000064
}

iaoverhaul.prestigiousemperor = {
	imperial_authority_value = 0.05000128
}
iaoverhaul.unprestigiousemperor = {
	imperial_authority_value = -0.10000128
}

iaoverhaul.powerfulemperor = {
	imperial_authority_value = 0.05000256
}
iaoverhaul.powerlessemperor = {
	imperial_authority_value = -0.10000256
}

iaoverhaul.absoluteempire = {
	imperial_authority_value = 0.10000512
}
iaoverhaul.stableempire = {
	imperial_authority_value = 0.05000512
}
iaoverhaul.unstableempire = {
	imperial_authority_value = -0.10000512
}



iaoverhaul.emperorovershadowed = {
	imperial_authority_value = -0.10001024
}



iaoverhaul.hegemon = {
	imperial_authority_value = 0.10004096
}
iaoverhaul.greatpower = {
	imperial_authority_value = 0.05004096
}



iaoverhaul.occupiedcapital = {
	imperial_authority_value = -0.20008192
}
iaoverhaul.siegedcapital = {
	imperial_authority_value = -0.10008192
}



iaoverhaul.goldenage = {
	imperial_authority_value = 0.10016384
}

iaoverhaul.reputableemperor = {
	imperial_authority_value = 0.05032768
}

iaoverhaul.waragainstforeigngreatpower = {
	imperial_authority_value = 0.05065536
}

iaoverhaul.regency = {
	imperial_authority_value = -0.10131072
}

iaoverhaul.toofewmembers = {
	imperial_authority_value = -0.20262144
}

iaoverhaul.bankruptcy = {
	imperial_authority_value = -0.20524288
}