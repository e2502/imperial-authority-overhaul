NDefines.NDiplomacy.HRE_PRINCE_AUTHORITY_THRESHOLD = 25		
NDefines.NDiplomacy.LACK_OF_ELECTORS_HIT = -0.10				-- Also applied to vassalized electors
NDefines.NDiplomacy.IMPERIAL_AUTHORITY_FROM_PRINCES = 0.00 		-- Handled through event modifiers
NDefines.NDiplomacy.HRE_FOREIGN_CONTROL_PENALTY = 0.00			-- Handled through event modifiers
NDefines.NDiplomacy.HRE_HERETIC_PENALTY = 0.00					-- Handled through event modifiers
NDefines.NDiplomacy.HRE_VOTE_TOO_SMALL = -50 					-- Doubled
NDefines.NDiplomacy.HRE_VOTE_BIG_COUNTRY = 50 					-- Doubled
NDefines.NDiplomacy.HRE_VOTE_VERY_BIG_COUNTRY = 100 			-- Doubled